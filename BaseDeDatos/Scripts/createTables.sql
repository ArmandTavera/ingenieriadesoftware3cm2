use upev_dev;

create table SequelizeMeta(
name varchar(255) not null primary key,
unique(name)
);

create table Responsables(
idResponsable int(11) not null auto_increment primary key,
nombre varchar(50) not null,
puesto varchar(25) not null,
extension varchar(7) not null,
celular varchar(10) not null,
correoInstitucional varchar(45) not null,
correoAlternativo varchar(45) not null,
tipo varchar(1),
createdAt datetime not null,
updatedAt datetime not null
);

create table PlanEstudios(
idPlanEstudios int(11) not null auto_increment primary key,
version varchar(10) not null,
createdAt datetime not null,
updatedAt datetime not null
);

create table ProgramasAcademicos(
idProgramaAcademico int(11) not null auto_increment primary key,
nombre varchar(50) not null,
idPlanEstudios int(11) not null,
foreign key(idPlanEstudios) references PlanEstudios(idPlanEstudios)
on update cascade on delete cascade,
createdAt datetime not null,
updatedAt datetime not null
);

create table UnidadesAprendizaje(
idUnidadAprendizaje int(11) not null auto_increment primary key,
nombre varchar(50),
idPlanEstudios int(11) not null,
foreign key(idPlanEstudios) references PlanEstudios(idPlanEstudios)
on update cascade on delete cascade,
createdAt datetime not null,
updatedAt datetime not null
);

create table UnidadesAcademicas(
idUnidadAcademica int not null auto_increment primary key,
nombre varchar(50),
createdAt datetime not null,
updatedAt datetime not null
);

create table Solicitudes(
idSolicitud int(11) not null auto_increment primary key,
fechaRecepcion datetime not null,
descripcion text not null,
idUnidadAcademica int(11) not null,
foreign key(idUnidadAcademica) references UnidadesAcademicas(idUnidadAcademica)
on update cascade on delete cascade,
idUnidadAprendizaje int(11) not null,
foreign key(idUnidadAprendizaje) references UnidadesAprendizaje(idUnidadAprendizaje)
on update cascade on delete cascade,
createdAt datetime not null,
updatedAt datetime not null
); 

create table Res_Sol(
idResponsable int(11) not null,
foreign key(idResponsable) references Responsables(idResponsable)
on update cascade on delete cascade,
idSolicitud int(11) not null,
foreign key(idSolicitud) references Solicitudes(idSolicitud)
on update cascade on delete cascade,
createdAt datetime not null,
updatedAt datetime not null
);